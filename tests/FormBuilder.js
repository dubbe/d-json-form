 /**
  * Unit tests
  */
describe("FormBuilder", function() {

    it("should be an object when initialized", function() {

      var jsonString = "{"
        + "\"form\": {"
        +  "\"name\": \"testar\""
        +"}"
      +"}";

        var formBuilder = new DForm(jsonString);

        // Assert
        expect(typeof formBuilder).toBe("object");

    });
});

describe("this.json", function () {

    it("should be a json object when json is a json-string", function () {
        var jsonString = "{"
          + "\"form\": {"
          +  "\"name\": \"testar\""
          +"}"
        +"}";
        var formBuilder = new DForm(jsonString);

        // Assert
        expect(typeof formBuilder.json).toBe("object");
        expect(formBuilder.json.form.name).toBe("testar");
    });

    it("should be a json object when json is a json-object", function () {
        var jsonObject = {
          "form": {
            "name": "testar"
          }
        };
        var formBuilder = new DForm(jsonObject);

        // Assert
        expect(typeof formBuilder.json).toBe("object");
        expect(formBuilder.json.form.name).toBe("testar");
    });


    it("should be undefined when json is not a parsable json-string", function () {
        var jsonString = "{"
          + "\"form\": {"
          +  "\"name\": \"testar\""
        +"}";
        var formBuilder = new DForm(jsonString);

        // Assert
        expect(typeof formBuilder.json).toBe("undefined");
    });
});

describe("this.form", function () {
    it("should have the name testar", function() {
      var jsonObject = {
        "form": {
          "name": "testar"
        }
      };
      var formBuilder = new DForm(jsonObject);

      // Assert
      expect(typeof formBuilder.json).toBe("object");
      expect(formBuilder.form.name).toBe("testar");
    })

    it("should not have the name testar", function() {
      var jsonObject = {
        "form": {
        }
      };
      var formBuilder = new DForm(jsonObject);

      // Assert
      expect(typeof formBuilder.json).toBe("object");
      expect(formBuilder.form.name).toBe(undefined);
    })
})
