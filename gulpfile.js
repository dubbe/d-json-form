var gulp = require('gulp'),
  jshint = require('gulp-jshint'),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
  jasmine = require('gulp-jasmine'),
  karma = require('karma').server,
  browserify = require('browserify'),
  transform = require('vinyl-transform'),
  typescript = require('gulp-typescript');

gulp.task('default', function() {

});

/**
 * Build js
 */

gulp.task('ts', function () {


  return gulp.src('source/ts/**/*.ts')
    //.pipe(browserified)
    .pipe(typescript({
      sourceMap: false, // (optional) Generates corresponding .map file.
      target: 'ES5', // (optional) Specify ECMAScript target version: 'ES3' (default), or 'ES5'
      module: 'amd', // (optional) Specify module code generation: 'commonjs' or 'amd'
      noImplicitAny: false, // (optional) Warn on expressions and declarations with an implied 'any' type.
      noResolve: true, // (optional) Skip resolution and preprocessing.
      removeComments: true // (optional) Do not emit comments to output.
    }))
    .pipe(concat('d-json-form.js'))
    .pipe(gulp.dest('build'));
});

gulp.task('ts-min', function () {


  return gulp.src('source/ts/**/*.ts')
    //.pipe(browserified)
    .pipe(typescript({
      sourceMap: false, // (optional) Generates corresponding .map file.
      target: 'ES5', // (optional) Specify ECMAScript target version: 'ES3' (default), or 'ES5'
      module: 'amd', // (optional) Specify module code generation: 'commonjs' or 'amd'
      noImplicitAny: false, // (optional) Warn on expressions and declarations with an implied 'any' type.
      noResolve: true, // (optional) Skip resolution and preprocessing.
      removeComments: true // (optional) Do not emit comments to output.
    }))
    .pipe(uglify())
    .pipe(concat('d-json-form.js'))
    .pipe(gulp.dest('build'));
});

gulp.task('watch', function() {
  // Do stuff
  gulp.watch('source/ts/**/*.ts', ['ts']);
});

/**
 * Run test once and exit
 */
 gulp.task('watchTest', function() {
   return karma.start({
     configFile: __dirname + '/karma.conf.js'
   });
 });

 gulp.task('test', function (done) {
   return karma.start({
     configFile: __dirname+'/karma.conf.js',
     singleRun: true
   }, done);
 });
