module.exports = function(config) {
  config.set({
    files: [
      'tests/**/*.js'
      ,'source/ts/**/*.ts'
    ],
    frameworks: ['jasmine'],
    preprocessors: {
      //'tests/**/*.js': ['browserify']
      'source/ts/**/*.ts': ['typescript']
    },
    typescriptPreprocessor: {
      // options passed to the typescript compiler
      options: {
        sourceMap: false, // (optional) Generates corresponding .map file.
        target: 'ES5', // (optional) Specify ECMAScript target version: 'ES3' (default), or 'ES5'
        module: 'commonjs', // (optional) Specify module code generation: 'commonjs' or 'amd'
        noImplicitAny: false, // (optional) Warn on expressions and declarations with an implied 'any' type.
        noResolve: false, // (optional) Skip resolution and preprocessing.
        removeComments: true // (optional) Do not emit comments to output.
      },
      // transforming the filenames
      transformPath: function(path) {
        return path.replace(/\.ts$/, '.js');
      }
    },
    browsers: ['PhantomJS'],
    reporters: ['spec'],
    /*browserify: {
      debug: true,
      transform: [ 'brfs' ]
    },*/
    //logLevel: 'LOG_DEBUG'
  });
};
