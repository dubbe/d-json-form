class BaseElement extends BaseObject {
  constructor(json: any, framework: string, increment?: number) {
    console.log("BASE ELEMENT " + increment)
    this.type = "div";
    this.className = "BaseElement";
    // Does super stuff...
    super(json, framework, increment);
  }
}
