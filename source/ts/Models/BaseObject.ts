/// <reference path="Attribute.ts" />
/// <reference path="../Views/ViewFactory.ts" />

class BaseObject {
  className: string;
  name: string;
  type: string;
  framework: string;
  attributes: { [index: string]: Attribute; } = {};
  row: any;
  column: any;
  elements: any;
  view: any;

  increment: number = 0;

  private _element: any;
  get element(): any {

    console.log(this.attributes);

    this._element = ViewFactory.GetView(this.framework, this.className, this.type, this.attributes).element;


    // Row
    if(this.row) {

      if(!this.row.repeat) {
        this.row.repeat = 1;
      }

      for (var n=0;n<=this.row.repeat-1;n++) {
        console.log("n: " + n);
        this._element.appendChild(new Row(this.row, this.framework, n).element);
      }

    }

    // Column
    if(this.column) {
      this._element.appendChild(new Column(this.column, this.framework, this.increment).element);
    }

    // Elements
    if(this.elements) {
      var that = this;
      this.elements.forEach(function(e) {
        var baseElement;
        // String creation of objects
        if(typeof e === "string") {
          e = {
            "type": e
          }
        }

        // If no type present
        var key = Object.keys(e)[0];
        if(typeof e.type == "undefined") {
          e[key].type = key.charAt(0).toUpperCase() + key.slice(1);
          if(typeof window[key] == "function") {
            baseElement = new window[key](e[key], that.framework, that.increment);
          } else {
            baseElement = new BaseElement(e[key], that.framework, that.increment);
          }
        } else {
          baseElement = new BaseElement(e[key], that.framework, that.increment);
        }

        that._element.appendChild(baseElement.element);
      })
    }

    return this._element;
  }
  constructor(json: any, framework: string, increment?: number) {
    this.framework = framework;

    if(this.className === undefined) {
      this.className = "BaseObject";
    }

    console.log(increment);
    if(increment) {
      this.increment = increment;
    }

    console.log("--- creating object " + this.className + ": ");


    if (json) {

      // Type?
      if(json.type) {
        this.type = json.type;
      }

      // Attributes
      if(json.name) {
        this.addAttribute("name", json.name);
        this.name = json.name;
      }
      if(json.id) {
        this.addAttribute("id", json.id);
      }
      if(json.class) {
        this.addAttribute("class", json.class);
      }

      if(json.row) {
        this.row = json.row;
      }

      if(json.column) {
        this.column = json.column;
      }

      if(json.elements) {
        this.elements = json.elements;
      }

    }

  }

  private _addAttribute(name: string, value: string) {
    value = this.replaceVariables(value);

    if(this.attributes === undefined) {
      this.attributes = {};
    }
    if(this.attributes[name]) {
      this.attributes[name].appendValue(value);
    } else {
      this.attributes[name] = new Attribute(name, value);
    }
  }
  addAttribute(name: string, value: any) {
    var that = this;
    if(typeof value == "object") {
      value.forEach(function(v) {
        	that._addAttribute(name, v);
      });
    } else {
      this._addAttribute(name, value);
    }



  }

  replaceVariables(value: string): string {
    var replaceStr = "{increment}";
    var regexp = new RegExp(replaceStr,"gi");
    var increment = this.increment != undefined ? this.increment.toString() : "";
    value = value.replace(regexp, increment);
    return value;
  }

}
