/// <reference path="BaseObject.ts" />

class Form extends BaseObject {
  /*className: string = "Form";
  type: string = "form";*/
  constructor(json: any, framework: string, className?: string) {
    this.className = "Form";
    this.type = "form";
    // Does super stuff...
    super(json, framework);
  }

}
