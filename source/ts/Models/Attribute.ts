class Attribute {
  name: string = "";
  value: string = "";
  constructor(name: string, value: any) {
    this.name = name;

    if(value.constructor === Array) {
      value.forEach(v => {
        this.value = this.value + v + " ";
      });

      // Remove trailing space
      if (this.value.substring(this.value.length-1) == " ")
      {
        this.value = this.value.substring(0, this.value.length-1);
      }


    } else if (typeof value == "string") {
      this.value = value;
    }

  }
  appendValue(value: string) {

    if (this.value.substring(this.value.length-1) !=" ")
    {
      this.value = this.value + " ";
    }

    this.value = this.value + value;
  }
}
