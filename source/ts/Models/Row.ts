/// <reference path="BaseObject.ts" />

class Row extends BaseObject {
  constructor(json: any, framework: string, increment?: number) {

    this.type = "div";
    this.className = "Row";

    this.addAttribute("class", "row");
    // Does super stuff...
    super(json, framework, increment);

  }

}
