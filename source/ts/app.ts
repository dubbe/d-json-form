/// <reference path="Models/Form.ts" />

class DForm {
  json: any;
  parent: any;
  form: any;
  framework: string = "Default";

  /* Constructor */
  constructor(_json: any, _parent: any);
  constructor(_json: string, _parent: any) {

    if(typeof _json == "string") {
      try {
        this.json = JSON.parse(_json);
      } catch (e) {
        this.json = undefined;
      }
    } else if(typeof _json == "object") {
      this.json = _json;
    } else {
      this.json = undefined;
    }



    // Do stuff with the json object
    if(typeof this.json == "object") {
      if(this.json.framework) {
        this.framework = this.json.framework;
      }
      this.buildForm();
    }

    if(_parent) {
      this.parent = _parent;
      // Draw the form thingie
      this.parent.appendChild(this.form.element);
    }

  }
  buildForm() {
    this.form = new Form(this.json.form, this.framework);
  }


  extendJquery() {
    var that = this;
    if(window["jQuery"] == undefined) {
      return;
    }
    window["jQuery"].fn.extend({
      dForm: function() {
        return this.each(function() {
          this.append("testar");
        })
      }
    })
  }
}
