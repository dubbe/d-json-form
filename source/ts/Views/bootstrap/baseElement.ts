module Bootstrap {
  export class BaseElement extends BaseView {

    get element(): any {
      var id, name;

      if(this.type == undefined && this.type == "") {
          this.type = "input";
      }


      if(this.attributes["id"]) {
        id = this.attributes["id"].value;
      }

      if(this.attributes["name"]) {
        name = this.attributes["name"].value;
      }

      // Create form-group wrapper as base
      this._element = document.createElement("div");
      this._element.setAttribute("class", "form-group");

      // Create label?
      var label = document.createElement("label");
      if(id) {
        label.setAttribute("for", id);
      }
      label.innerHTML = name + ":";
      this._element.appendChild(label);

      // Create element
      var element = document.createElement(this.type);
      if(this.attributes != undefined) {
        for (var key in this.attributes) {
          element.setAttribute(this.attributes[key].name, this.attributes[key].value);
        }
        element.setAttribute("class", "form-control");
      }

      this._element.appendChild(element);

      return this._element;
    }

    constructor(type: string, name: string, attributes: { [index: string]: Attribute; }) {
      console.log("Element!!!");
      super(type, name, attributes);
    }
  }
}
