module Bootstrap  {
  export class Row extends BaseView {
    type: string;
    name: string;
    attributes: { [index: string]: Attribute; } = {};

    _element: any;
    get element(): any {
      if(this._element === undefined) {
        if(this.type != undefined && this.type != "") {
          this._element = document.createElement(this.type);
          if(this.attributes != undefined) {
            for (var key in this.attributes) {
              this._element.setAttribute(this.attributes[key].name, this.attributes[key].value);
            }
            this._element.setAttribute("class", "row");
          }
        }
      }
      return this._element;
    }

    constructor(type: string, name: string, attributes: { [index: string]: Attribute; }) {
      super(type, name, attributes);
    }
  }
}
