///<reference path='bootstrap/baseView.ts'/>

///<reference path='default/baseView.ts'/>

class ViewFactory {
  static GetView(framework: string, caller: string, type: string, attributes: { [index: string]: Attribute; }): any {
    console.log("-- Get view: " + caller);
    console.log("-- Framework: "+ framework);

    console.log(typeof window["Bootstrap"]);
    var ns, view;
    if (typeof window[framework] === 'object') {
      ns = window[framework];
    } else {
      ns = Default;
    }

    if(typeof ns[type] === 'function') {
      view = new ns[type](type, name, attributes)
    } else if(typeof ns[caller] === 'function') {
      view = new ns[caller](type, name, attributes)
    } else {
      view = new ns.BaseView(type, name, attributes)
    }

    return view;
  }
}
