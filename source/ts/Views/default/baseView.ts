/// <reference path="../../models/Attribute.ts" />
module Default {
  export class BaseView {
    type: string;
    name: string;
    attributes: { [index: string]: Attribute; } = {};

    private _element: any;
    get element(): any {
      if(this._element === undefined) {
        if(this.type != undefined && this.type != "") {
          this._element = document.createElement(this.type);
          if(this.attributes != undefined) {
            for (var key in this.attributes) {
              this._element.setAttribute(this.attributes[key].name, this.attributes[key].value);
            }
          }
        }
      }
      return this._element;
    }

    constructor(type: string, name: string, attributes: { [index: string]: Attribute; }) {
      this.type = type;
      this.name = name;
      this.attributes = attributes;
    }
  }
}
