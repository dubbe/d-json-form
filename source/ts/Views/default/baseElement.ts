module Default {
  export class BaseElement extends BaseView {
    constructor(type: string, name: string, attributes: { [index: string]: Attribute; }) {
      console.log("Default Element");
      super(type, name, attributes);
    }
  }
}
